<!DOCTYPE html> 
<html>
<head>
<meta charset="UTF-8">
<title>Le miel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/animate.css" />
</head>
<body>
<main>

	<header>
		&nbsp;
		<h1>Le miel</h1>
	</header>

	<nav id='ext'>
		<ul>
			<li><a href='https://fr.wikipedia.org/wiki/Miel'>Le miel sur wikipedia</a>
			<li><a href='http://www.mielinfrance.fr/miel-et-apiculture/bienfaits-du-miel/'>Les biensfaits du miel</a>
			<li><a href='https://www.saveur-biere.com/fr/biere-bouteille/103-barbar-au-miel.html'>Barbar : Bière au miel</a>
		</ul>
	</nav>

	<nav id='int'>
		<ul>
			<li><a href='#definition'>Le miel</a></li>
			<li><a href='#images'>Images</a></li>
			<li><a href=''></a></li>
			<li><a href=''></a></li>
			<li><a href=''></a></li>
			<li><a href=''></a></li>
			<li><a href=''></a></li>
			<li><a href=''></a></li>
		</ul>
	</nav>

	<section>
		<article id='definition'>
			<h2>Qu'est ce que le miel ?</h2>
			<p>
				Les abeilles butineuses sont chargées de l'approvisionnement de la ruche. Une fois posée sur une plante à fleurs (angiospermes), l'abeille en écarte les pétales, plonge sa tête à l'intérieur, allonge sa langue et aspire le nectar qu'elle stocke provisoirement dans son jabot. Du fait de leur anatomie et en particulier de la longueur de leur langue, les abeilles ne peuvent récolter le nectar que sur certaines fleurs, qui sont dites alors mellifères.
			</p>
			<p>
				Les abeilles peuvent aussi récolter du miellat, excrétion produite par des insectes suceurs comme le puceron, la cochenille ou le metcalfa à partir de la sève des arbres. Il sera utilisé de la même façon que le nectar de fleur (c'est ce produit de base qui est notamment utilisé pour élaborer le miel de sapin).
			</p>
			<p>
				L’élaboration du miel commence dans le jabot de l’ouvrière, pendant son vol de retour vers la ruche. L’invertase, une enzyme de la famille des diastases, est ajoutée, dans le jabot, au nectar. Il se produit alors une réaction chimique, l’hydrolyse du saccharose qui donne du glucose et du fructose.
			</p>
			<p>
				Arrivée dans la ruche, l’abeille butineuse régurgite le nectar à une receveuse (trophallaxie), qui, à son tour, régurgitera et ré-ingurgitera ce nectar riche en eau, en le mêlant à de la salive et à des sucs digestifs, ayant pour effet de compléter le processus de digestion des sucres. Une fois stocké dans les alvéoles, le miel est déshydraté par une ventilation longue et énergique de la part précisément des ouvrières ventileuses. Parvenu à maturité, le miel a une durée de conservation extrêmement longue.
			</p>
			<p>
				La chaleur de la ruche ainsi que les ouvrières ventileuses, qui peuvent entretenir un courant d’air pendant 20 minutes dans la ruche, provoquent l’évaporation de l’eau. Le miel arrive à maturité lorsque sa teneur en eau devient inférieure à 18 % ; il est alors emmagasiné dans d’autres alvéoles qui seront operculés une fois remplis.
			</p>
			<p>
				Le miel est ainsi stocké par les abeilles pour servir de réserve de nourriture ; en particulier pendant les saisons défavorables, en saison sèche pour les Apis dorsata ou l'hiver pour les Apis mellifera.
			</p>
			<p>
				Le scientifique Bernd Heinrich a mesuré le volume de travail effectué par les abeilles butineuses. Ainsi, pour produire une livre de miel, les abeilles doivent effectuer plus de 17 000 voyages, visiter 8 700 000 fleurs, le tout représentant plus de 7 000 heures de travail7.
			</p>
		</article>

		<article id='images'>
			<h2>Les fleurs et les danses des abeilles</h2>
			<div id='imagesContainer'>
				<div class='bof'><img src='images/bee1.jpg' alt='bee'/></div>
				<div class='bof'><img src='images/bee2.jpg' alt='bee'/></div>
				<div class='bof'><img src='images/bee1.jpg' alt='bee'/></div>
				<div class='bof'><img src='images/bee2.jpg' alt='bee'/></div>
				<div class='bof'><img src='images/bee1.jpg' alt='bee'/></div>
				<div class='bof'><img src='images/bee2.jpg' alt='bee'/></div>
				<div class='bof'><img src='images/bee1.jpg' alt='bee'/></div>
				<div class='bof'><img src='images/bee2.jpg' alt='bee'/></div>
				<div class='bof'><img src='images/bee1.jpg' alt='bee'/></div>
				<div class='bof'><img src='images/bee2.jpg' alt='bee'/></div>
			</div>
		</article>
	</section>

	<footer>
		CC BY NC SA - <a href='http://www.luc-damas.fr'>Luc Damas</a> - 2017
	</footer>

</main>

<script type='text/javascript' src='js/jquery.js'></script>
<script type='text/javascript' src='js/jquery-ui.js'></script>
<script type='text/javascript' src='js/main.js'></script>
</body>
</html>
