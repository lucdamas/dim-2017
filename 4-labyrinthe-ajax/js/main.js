
var url = "http://srv-tpinfo.iut-acy.local:1337/";

$(document).ready(function() {

	// Récup du labyrinthe
	$.post(
		url+"lab",
		{ pseudo: "ldama", password: "7369" },
		function(maze) {

			var mazeDiv = $("<div/>");
			mazeDiv.attr("id","mazeDiv");
			$("body").append(mazeDiv);

			for(var y=0; y<maze.length; y++) {
				for(var x=0; x<maze[y].length; x++) {
					var element = $("<div/>");
					if (maze[y][x] == 1) {  // Wall = 1
						element.addClass("wall");
					} else {                // Floor = 0
						element.addClass("floor");
					}
					if (x==0)
						element.addClass("lineEnd");
					mazeDiv.append(element);
				}
			}

			setInterval(play, 10);

		},
		"json"
	);


	// Gestion des événements claviers
	$("body").keyup(function(event) {
		switch(event.keyCode) {
			case 38: move( 0, -1); break;
			case 40: move( 0,  1); break;
			case 37: move(-1,  0); break;
			case 39: move( 1,  0); break;
		};
	});


});

//------------------------------------------
var speed = { x: 0, y: 0 };

function move(vx, vy) {
	speed.x = vx;
	speed.y = vy;
}

//------------------------------------------
var count = 0;
function play() {
	if (count == 0)
		players();
	else if (count == 1)
		moveMe();
	else
		coin();
	count = (count+1)%3;
}

//------------------------------------------
function moveMe() {
	// if (speed.x ==0 && speed.y == 0) {
		speed.x = coinPos.x - me.x;
		if (speed.x != 0)
			speed.x = speed.x/Math.abs(speed.x); 
		speed.y = coinPos.y - me.y;
		if (speed.y != 0)
			speed.y = speed.y/Math.abs(speed.y); 
		console.log(speed);
	// }
	$.post(
		url+"move",
		{ 
			pseudo: "ldama", 
			password: "7369",
			v: speed
		},
		function(response) {

			speed = { x: 0, y: 0 };
		},
		"json"
	);
}

//------------------------------------------
var me = { x: 0, y: 0 };
function players() {
	$.post(
		url+"players",
		{ pseudo: "ldama", password: "7369" },
		function(players) {
			for(var pseudo in players) {
				var player = players[pseudo];
				var playerDiv;
				if ($("#"+pseudo).length > 0) {
					playerDiv = $("#"+pseudo);	
				} else {
					playerDiv = $("<div/>");
					playerDiv
						.attr("id",pseudo)
						.addClass("player")
						.addClass(pseudo=="ldama"?"me":"opponent")
						.append("<div class='playerPseudo'>"
							     +pseudo+" "+player.score+"</div>");
					$("#mazeDiv").append(playerDiv);
				}
				playerDiv.children(".playerPseudo")
					.text(pseudo+" "+player.score);
				if (pseudo == "ldama")
					me = player.position;
				playerDiv.css({
					top: player.position.y*10,
					left: player.position.x*10,
				});
			}
		},
		"json"
	);
}

//------------------------------------------
var coinPos = { x: 0, y: 0 };
function coin() {
	$.post(
		url+"coin",
		{ pseudo: "ldama", password: "7369" },
		function(coinP) {
			coinPos = coinP;
			var coinDiv;
			if ($("#coin").length > 0) {
				coinDiv = $("#coin");	
			} else {
				coinDiv = $("<div/>");
				coinDiv
					.attr("id","coin")
				$("#mazeDiv").append(coinDiv);
			}
			coinDiv.css({
				top: coinPos.y*10,
				left: coinPos.x*10,
			});
		},
		"json"
	);
}