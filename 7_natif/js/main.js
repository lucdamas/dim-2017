//-------------------------------------------------------------------
document.addEventListener("DOMContentLoaded", function() {
	document.querySelector("#link-home").onclick = displayHome;
	document.querySelector("#link-admin").onclick = displayAdmin;
	displayHome();
});

//-------------------------------------------------------------------
function displayHome() {
	let content = document.querySelector("#content");
	content.innerHTML = "";

	let h1 = document.createElement("h1");
	h1.appendChild(document.createTextNode("Accueil"));
	content.appendChild(h1);

	let ul = document.createElement("ul");
	content.appendChild(ul);

	ajax("controller/service.php", { }, function(data) {
		for(let movie of data) {
			let li = document.createElement("li");
			li.appendChild(document.createTextNode(movie.title));
			ul.appendChild(li);
		}		
	});
}
//-------------------------------------------------------------------
function displayAdmin() {
	document.querySelector("#content").innerHTML = 
		"<h1>Admin</h1>"+
		"<p><label>Titre</label><input id='title'/></p>"+
		"<p><label>Réalisateur</label><input id='director'/></p>"+
		"<p><input id='add-movie' type='button' value='ajouter'/></p>";

	document.querySelector("#add-movie").onclick = function() {
		let title = document.querySelector("#title").value;
		let director = document.querySelector("#director").value;
		if (title != "" && director != "") {
			ajax("controller/service.php", {
				"action": "add",
				"title": title,
				"director": director
			}, function(response) {
				displayMessage("Film ajouté");
				displayHome();
			});
		}
	}


}

function displayMessage(message) {
	var section = document.querySelector("#content");
	section.insertAdjacentHTML('beforebegin',
	 "<div class='message'>"+message+"</div>");
	setTimeout(function() {
		var messages = document.querySelectorAll(".message");
		for(let message of messages)
			document.querySelector("body").removeChild(message);
	}, 1000);
}

//-------------------------------------------------------------------
function ajax(url, params, callback) {

	var xhr = new XMLHttpRequest();
	xhr.open("POST", url, true);
	xhr.responseType = "json";
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	xhr.onreadystatechange = function() {
		if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
			callback(xhr.response);
		}
	}
	var s = "";
	for(let field in params)
		s += field+"="+params[field]+"&";
	s = s.substr(0, s.length-1);

	xhr.send(s); 

}
