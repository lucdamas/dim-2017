<?php
$data = json_decode(file_get_contents("data.json"), true);

if (isset($_POST["action"])) {

	if ($_POST["action"] == "add") {
		$data[] = array(
			"title" => $_POST["title"],
			"director" => $_POST["director"]
		);
		file_put_contents("data.json", json_encode($data));
		echo json_encode(array("ok"=>true));
	}


} else {

	echo json_encode($data);
	
}



