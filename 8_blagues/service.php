<?php


$filename = "data.json";
if (!file_exists($filename))
	file_put_contents($filename, '{ "users": [], "jokes": [] }');

$data = json_decode(file_get_contents($filename), true);

$params = array();
foreach($_POST as $k=>$v) $params[$k] = $v;
foreach($_GET as $k=>$v) $params[$k] = $v;

if (isset($params["action"])) {

	//-------------------------------------------- joke-read
	if ($params["action"] == "joke-read") {
		echo json_encode($data["jokes"]);
	}
	//-------------------------------------------- joke-create
	else if ($params["action"] == "joke-create") {
		if (isset($params["text"]) && isset($params["user"])) {
			$text = str_replace('"',"'",strip_tags($params["text"]));
			$user = str_replace('"',"'",strip_tags($params["user"]));
			$data["jokes"][] = array(
				"id"   => round(10000*microtime(true)),
				"text" => $text,
				"user" => $user
			);
			file_put_contents($filename, json_encode($data));
			echo json_encode(array("ok"=>"joke-create"));
		} else
			echo json_encode(array("error"=>"joke-create: text, user required"));
	}
	//-------------------------------------------- joke-reset
	else if ($params["action"] == "joke-reset") {
		$data["jokes"] = array();
		file_put_contents($filename, json_encode($data));
		echo json_encode(array("ok"=>"joke-reset"));
	}
	//-------------------------------------------- joke-delete
    else if ($params["action"] == "joke-delete") {
        $ids = array_map(function($element) {return $element['id'];}, $data["jokes"]);
        if (isset($params["id"])) {
            $key = array_search($params["id"], $ids);
            if ($key === false)
                echo json_encode(array("error"=>"ID unknown (delete)"));
            else {
                array_splice($data["jokes"],$key,1);
                file_put_contents($filename, json_encode($data));
                echo json_encode(array("ok"=>"joke-delete"));
            }
        } else
            echo json_encode(array("error"=>"ID required (delete)"));        
    }

	//-------------------------------------------- joke-update
	else if ($params["action"] == "joke-update") {
		if (isset($params["id"]) && isset($params["text"])) {
			$key = array_search($params["id"], array_column($data["jokes"], 'id'));
			if ($key === false)
				echo json_encode(array("error"=>"ID unknown (update)"));
			else {
				$data["jokes"][$key]["text"] = $params["text"];
				file_put_contents($filename, json_encode($data));
				echo json_encode(array("ok"=>"joke-update"));
			}
		} else
			echo json_encode(array("error"=>"ID and text required (update)"));		
	}
	

	

	//---------------------------------------------Unknwon action
	else {

		echo json_encode(array("error"=>"Bad action: "
								.$params["action"]));
		
	}





} else {

	echo json_encode(array("error"=>"No action specified"));
	
}





