
$(document).ready(function() {

	let url = "service.php";

	$("#button-jokes").click(allJokes);
	$("#button-admin").click(startAdmin);
	$("#admin").hide();
	$("#button-jokes").addClass("selected");

	if (!cookie("page"))
		cookie("page","home");

	if (cookie("page") == "home")
		allJokes();
	else if (cookie("page") == "admin")
		startAdmin();
	else
		allJokes();
	



	function cookie(name, value) {
		if (value) {
			document.cookie = name+"="+value;
		} else {
			let namePos = document.cookie.indexOf(name);
			let value = null;
			if (namePos != -1) {
		 		let semicolonPos = document.cookie.indexOf(";",namePos);
		 		if (semicolonPos == -1)
		 			semicolonPos = document.cookie.length;
				value = document.cookie.substr(namePos+name.length+1, semicolonPos-namePos-name.length-1);
			}
			return value;
		}
	}

	function allJokes() {
		$("#admin").hide();
		$("#jokes").show();
		$("#button-jokes").toggleClass("selected");
		$("#button-admin").toggleClass("selected");
		displayAllJokes();
		cookie("page","home");
	}
	function startAdmin() {
		$("#admin").show();
		$("#jokes").hide();
		$("#button-jokes").toggleClass("selected");
		$("#button-admin").toggleClass("selected");
		displayAdmin();
		cookie("page","admin");
	}

	function displayAllJokes() {
		$.get(
			url,
			{ action: "joke-read" },
			function(jokes) {
				$("#all-jokes").empty();
				for(let joke of jokes) {
					let jokeDiv = $("<div/>");
					jokeDiv.data("joke",joke);
					jokeDiv.addClass("joke");
					$("#all-jokes").append(jokeDiv);
					let p = $("<p/>");
					p.addClass("joke-text");
					p.append(joke.text);
					jokeDiv.append(p);
					p = $("<p/>");
					p.addClass("joke-user");
					p.append('<i class="fa fa-user" aria-hidden="true"></i> ' + joke.user);
					jokeDiv.append(p);

				}
			},
			"json"
		);

	}


	var addButtonFlag = false;

	function displayAdmin() {
		//---------------------------------------------
		if (!addButtonFlag)
			$("#add-joke-button")
				.click(function() {
					let text = $("#joke-text").val().trim();
					if (text != "") {
						$.get(
							url,
							{ action: "joke-create",
							  text: text,
							  user: "Luc" },
							function(data) {
								$("#joke-text").val("");
							},
							"json");
						allJokes();
					}
					addButtonFlag = true;
				});
		//-----------------------------------------------
		$.get(
			url,
			{ action: "joke-read" },
			function(jokes) {
				$("#admin #jokes").empty();
				for(let joke of jokes) {
					let jokeDiv = $("<div/>");
					jokeDiv.data("joke",joke);
					jokeDiv.addClass("joke-to-delete");
					jokeDiv.append(joke.text.substr(0,10)+"...");
					jokeDiv.draggable();
					$("#admin #jokes").append(jokeDiv);
				}
			}
			,"json");
		$("#trash").droppable({
			drop: function(event, ui) {
				ui.draggable.remove();
		
			}
		})

	}

});

