/*

Micro-lib d'authentification côté client avec vérification côté serveur.

Utilisation : 
- Inclusion CSS (auth.css);
- Inclusion JS (auth.js)
- Modif html éventuelle : class ou ID sur le lien à contrôler
- Appel fonction handleAuth(selector)

*/


var code = [];

function handleAuth(selector) {


	$(selector).click(function(event) {
		var link = $(this);
		event.preventDefault();

		$.get(
			"intranet/connect.php",
			{  },
			function(response) {

				if (response.connected) {
					window.location = link.attr("href");
				} else {

					var bg = $("<div/>");
					bg.attr("id","bg");
					$("body").append(bg);		

					var canvas = document.createElement("canvas");
					canvas.setAttribute("width","300px");
					canvas.setAttribute("height","300px");
					var c = canvas.getContext("2d");
					c.strokeStyle = "#AAAAAA";
					c.lineWidth = 3;
					c.fillStyle = "#AAAAAA";

					var authContainer = $("<div/>");
					authContainer
						.attr("id","authContainer")
						.hover(
							function() {
								$(this)
									.addClass("authContainerHover");
							},
							function() {
								$(this)
									.removeClass("authContainerHover");
								
								$.get(
									"intranet/connect.php",
									{ code: code.join("") },
									function(response) {
										if (response.connected) {
											$(".authButtonSelected").addClass("authButtonOK");

											setTimeout(function() {
												$(".authButton")
													.removeClass("authButtonOK")
													.removeClass("authButtonHover")
													.removeClass("authButtonSelected");

												window.location = link.attr("href");

											}, 400);


										} else {
											$(".authButtonSelected")
												.addClass("authButtonFail");
 
											setTimeout(function() {
												$(".authButton")
													.removeClass("authButtonFail")
													.removeClass("authButtonHover")
													.removeClass("authButtonSelected");
											}, 400);

										}
										code = [];
										c.clearRect(0,0,canvas.width, canvas.height);
									},
									"json");

					
							}
						);
					bg.append(authContainer);



					for(var i=0; i<9; i++) {
						var authButton = $("<div/>");
						authButton
							.addClass("authButton")
							.hover(
								function() {
									$(this)
										.addClass("authButtonHover")
										.addClass("authButtonSelected");
									code.push($(this).index());

									var center = {
										x: $(this).position().left+$(this).outerWidth(true)/2,
										y: $(this).position().top+$(this).outerHeight(true)/2
									};
									if (code.length == 1) {
										c.beginPath();
										c.moveTo(center.x,center.y);
											
									} else {
										c.lineTo(center.x,center.y);
										c.stroke();
									}
								},
								function() {
									$(this).removeClass("authButtonHover");
								}
							);
						authContainer.append(authButton);
					}

				}


				var authSuperContainer = $("<div/>");
				authSuperContainer.attr("id","authSuperContainer");
				authContainer.wrap(authSuperContainer);
				document.querySelector("#authSuperContainer")
										.appendChild(canvas);
					

			}, "json");

	});

}

