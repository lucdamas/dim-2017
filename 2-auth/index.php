<?php
session_start();
if (!isset($_SESSION["page"])) $_SESSION["page"] = "home";
if (isset($_GET["page"])) $_SESSION["page"] = $_GET["page"];
?>
<!DOCTYPE html> 
<html>
<head>
	<meta charset="UTF-8">
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/animate.css" />
	<link rel="stylesheet" type="text/css" href="css/auth.css" />
</head>
<body>

<header>
	<h1>DIM</h1>
</header>

<nav>
	<ul>
		<li><a href='.?page=home'>Accueil</a></li>
		<li><a href='.?page=list'>Liste</a></li>

		<li><a href='intranet' class='auth'>Intranet</a></li>
		
	</ul>	


</nav>



<section>

<?php 
if ($_SESSION["page"] == "home") {
?>
	<h2>Accueil</h2>
	<p>
		JS <3
	</p>

<?php
} else if ($_SESSION["page"] == "list") {
?>

	<h2>Liste</h2>
	<table>
		<tr><td>BOULET</td>	<td>Alexis</td></tr>
		<tr><td>CAVASIN</td>	<td>Florian</td></tr>
		<tr><td>COMINOTTI</td>	<td>Victor</td></tr>
		<tr><td>CREPEL</td>	<td>Alban</td></tr>
		<tr><td>DUPRE</td>	<td>Stephen</td></tr>
		<tr><td>DUPUY</td>	<td>Rémi</td></tr>
		<tr><td>FANCONY</td>	<td>Romain</td></tr>
		<tr><td>FRANCONY</td>	<td>Steven</td></tr>
		<tr><td>FRONTIERE</td> <td>	Rémi</td></tr>
		<tr><td>LAMIT</td>	<td>Benoit</td></tr>
		<tr><td>LEDENT</td>	<td>Loric</td></tr>
		<tr><td>LEROY</td>	<td>Emilien</td></tr>
		<tr><td>MACHEDA</td>	<td>Adam</td></tr>
		<tr><td>MASLOVA</td>	<td>Olga</td></tr>
		<tr><td>RICCHI</td>	<td>Evan</td></tr>
		<tr><td>ROUSSEL</td>	<td>Adrien</td></tr>
		<tr><td>TISON</td>	<td>Adrien</td></tr>
		<tr><td>TOMASI</td>	<td>François</td></tr>
		<tr><td>VOVARD</td>	<td>Roxane</td></tr>
	</table>

<?php
} else {
?>

	<h2>Erreur</h2>
	<p>
		Page inconnue...
	</p>
	<p>
		<a href='.?page=home'>Accueil</a>
	</p>	
<?php
}
?>




</section>

<footer>
	CC BY NC SA - Luc Damas
</footer>



<script type='text/javascript' src='js/jquery.js'></script>
<script type='text/javascript' src='js/auth.js'></script>
<script type='text/javascript' src='js/main.js'></script>
</body>
</html>
